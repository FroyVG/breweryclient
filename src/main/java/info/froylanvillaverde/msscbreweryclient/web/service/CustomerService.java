package info.froylanvillaverde.msscbreweryclient.web.service;

import info.froylanvillaverde.msscbreweryclient.web.client.BreweryClient;
import info.froylanvillaverde.msscbreweryclient.web.client.CustomerClient;
import info.froylanvillaverde.msscbreweryclient.web.model.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.UUID;

@Service
public class CustomerService {

    @Autowired
    private CustomerClient client;

    public CustomerDto getCustomerById(UUID uuid) {
        return client.getCustomerById(uuid);
    }

    public URI saveNewCustomer(CustomerDto customerDto) {
        return client.saveNewCustomer(customerDto);
    }

    public void updateCustomer(UUID uuid, CustomerDto customerDto) {
        client.updateCustomer(uuid, customerDto);
    }

    public void deleteCustomer(UUID id) {
        client.deleteCustomer(id);
    }
}
