package info.froylanvillaverde.msscbreweryclient.web.service;

import info.froylanvillaverde.msscbreweryclient.web.client.BreweryClient;
import info.froylanvillaverde.msscbreweryclient.web.model.BeerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.UUID;

@Service
public class BeerService {

    @Autowired
    private BreweryClient client;

    public BeerDto getBeerById(UUID uuid) {
        return client.getBeerById(uuid);
    }

    public URI saveNewBeer(BeerDto beerDto) {
        return client.saveNewBeer(beerDto);
    }

    public void updateBeer(UUID uuid, BeerDto beerDto) {
        client.updateBeer(uuid, beerDto);
    }

    public void deleteBeer(UUID id) {
        client.deleteBeer(id);
    }
}
