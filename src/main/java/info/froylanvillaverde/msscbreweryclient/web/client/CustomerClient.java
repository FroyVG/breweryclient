package info.froylanvillaverde.msscbreweryclient.web.client;

import info.froylanvillaverde.msscbreweryclient.web.model.CustomerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.UUID;

@Component
@ConfigurationProperties(prefix = "sfg.brewery")
@Slf4j
public class CustomerClient {

    private String apihost;
    private String customerpath;;
    private final RestTemplate restTemplate;


    public CustomerClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public CustomerDto getCustomerById(UUID id) {
        log.info("BEGIN - getCustomerById( {} )", id);
        CustomerDto customerDto = restTemplate.getForObject(apihost + customerpath + id.toString(), CustomerDto.class);
        log.info("END - getCustomerById = {}", customerDto);
        return customerDto;
    }

    public URI saveNewCustomer(CustomerDto customerDto) {
        return restTemplate.postForLocation(apihost + customerpath, customerDto);
    }

    public void updateCustomer(UUID uuid, CustomerDto customerDto) {
        restTemplate.put(apihost + customerpath + uuid.toString(), customerDto);
    }

    public void deleteCustomer(UUID uuid) {
        restTemplate.delete(apihost + customerpath + uuid);
    }

    public void setApihost(String apihost) {
        this.apihost = apihost;
    }

    public void setCustomerpath(String customerpath) {
        this.customerpath = customerpath;
    }
}
