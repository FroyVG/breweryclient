package info.froylanvillaverde.msscbreweryclient.web.client;

import info.froylanvillaverde.msscbreweryclient.web.model.BeerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.UUID;

@Component
@ConfigurationProperties(prefix = "sfg.brewery")
@Slf4j
public class BreweryClient {

    private String beerpath;
    private String apihost;
    private final RestTemplate restTemplate;

    public BreweryClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public BeerDto getBeerById(UUID uuid) {
        String endpoint = apihost + beerpath + uuid.toString();
        log.info("BEGIN - getBeerById {}", endpoint);
        return restTemplate.getForObject(endpoint, BeerDto.class);
    }

    public URI saveNewBeer(BeerDto beerDto) {
        return restTemplate.postForLocation(apihost + beerpath, beerDto);
    }

    public void updateBeer(UUID uuid, BeerDto beerDto) {
        restTemplate.put(apihost + beerpath + uuid.toString(), beerDto);
    }

    public void deleteBeer(UUID id) {
        restTemplate.delete(apihost + beerpath + id);
    }

    public void setApihost(String apihost) {
        this.apihost = apihost;
    }

    public void setBeerpath(String beerpath) {
        this.beerpath = beerpath;
    }
}
