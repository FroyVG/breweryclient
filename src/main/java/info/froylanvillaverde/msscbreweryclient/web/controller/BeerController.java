package info.froylanvillaverde.msscbreweryclient.web.controller;

import info.froylanvillaverde.msscbreweryclient.web.model.BeerDto;
import info.froylanvillaverde.msscbreweryclient.web.service.BeerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@RequestMapping("/api/v1/beer")
@RestController
@Slf4j
@Validated
public class BeerController {

    @Autowired
    private BeerService service;

    @GetMapping("/{beerId}")
    public ResponseEntity<BeerDto> getBeerById(@NotNull @PathVariable UUID beerId) {
        log.info("BEGIN - getBeerById( {} )", beerId);
        return new ResponseEntity<>(service.getBeerById(beerId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveNewBeer(@Valid @NotNull @RequestBody BeerDto beerDto) {
        log.info("saveNewBeer - {}", beerDto);
        service.saveNewBeer(beerDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{beerId}")
    public ResponseEntity updateBeerById(@PathVariable UUID beerId, @Valid @RequestBody BeerDto beerDto) {
        service.updateBeer(beerId, beerDto);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
