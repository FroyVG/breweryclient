package info.froylanvillaverde.msscbreweryclient.web.controller;

import info.froylanvillaverde.msscbreweryclient.web.model.CustomerDto;
import info.froylanvillaverde.msscbreweryclient.web.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequestMapping("/api/v1/customer")
@RestController
@Slf4j
@Validated
public class CustomerController {

    @Autowired
    private CustomerService service;

    @GetMapping("/{customerId}")
    public ResponseEntity<CustomerDto> getCustomerById(@Valid @PathVariable UUID customerId){
        log.info("BEGIN - getCustomerById( {} )", customerId);
        return new ResponseEntity<>( service.getCustomerById(customerId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveNewCustomer(@Valid @RequestBody CustomerDto customerDto){
        service.saveNewCustomer(customerDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{customerId}")
    public ResponseEntity updateCustomerById(@PathVariable UUID customerId, @Valid @RequestBody CustomerDto customerDto){
        service.updateCustomer(customerId, customerDto);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
